import { GraphQLClient } from 'graphql-request';
import { AbstractPage, Editor } from './pages/AbstractPage';
export declare type PageRuleMatch = 'START' | 'EXACT' | 'END' | 'REGEX' | 'TAG';
export declare type Permission = 'read:pages' | 'write:pages' | 'manage:pages' | 'delete:pages' | 'read:source' | 'read:history' | 'read:assets' | 'write:assets' | 'manage:assets' | 'read:comments' | 'write:comments' | 'manage:comments';
export interface PageRule {
    id: string;
    deny: boolean;
    match: PageRuleMatch;
    roles: Array<Permission>;
    path: string;
    locales: Array<string>;
}
export interface UserMinimal {
    id: number;
    name: string;
    email: string;
    providerKey: string;
    isSystem: boolean;
    isActive: boolean;
    createdAt: string;
    lastLoginAt: string | null;
}
export interface User {
    id: number;
    name: string;
    email: string;
    providerKey: string;
    providerId: string | null;
    isSystem: boolean;
    isActive: boolean;
    isVerified: boolean;
    location: string;
    jobTitle: string;
    timezone: string;
    dateFormat: string;
    appearance: string;
    createdAt: string;
    updatedAt: string;
    lastLoginAt: string;
    groups: Array<Group>;
}
export interface ListUsersResult {
    users: {
        list: Array<UserMinimal>;
    };
}
export interface UserResult {
    users: {
        single: User;
    };
}
export interface GroupMinimal {
    id: number;
    name: string;
    isSystem: boolean;
    userCount: number;
    createdAt: string;
    updatedAt: string;
}
export interface Group {
    id: number;
    name: string;
    isSystem: boolean;
    permissions: Array<Permission>;
    pageRules: Array<PageRule>;
    users: Array<UserMinimal>;
    createdAt: string;
    updatedAt: string;
}
export interface ListGroupsResult {
    groups: {
        list: Array<GroupMinimal>;
    };
}
export interface GroupResult {
    groups: {
        single: Group;
    };
}
export interface PageInfo {
    id: number;
    path: string;
    hash: string;
    title: string;
    description: string;
    isPrivate: boolean;
    isPublished: boolean;
    content: string;
    editor: Editor;
}
export interface PageInfoResult {
    pages: {
        single: PageInfo;
    };
}
export interface ResponseStatus {
    succeeded: boolean;
    errorCode: number;
    slug: string;
    message: string;
}
export interface CreatePageResult {
    pages: {
        create: {
            responseResult: ResponseStatus;
        };
    };
}
export interface UpdatePageResult {
    pages: {
        update: {
            responseResult: ResponseStatus;
        };
    };
}
/**
 * Used to interact with Wiki.js GraphQL API
 */
export declare class WikiClient {
    client: GraphQLClient;
    constructor(url: string, token: string);
    /**
     * List users by filter
     * @param filter
     * @param orderBy
     */
    listUsers(filter?: string, orderBy?: string): Promise<ListUsersResult>;
    /**
     * Get information about specified user
     * @param id user id
     */
    showUser(id: number): Promise<UserResult>;
    /**
     * List groups by filter
     * @param filter
     * @param orderBy
     */
    listGroups(filter?: string, orderBy?: string): Promise<ListGroupsResult>;
    /**
     * Get information about specified group
     * @param id group id
     */
    showGroup(id: number): Promise<GroupResult>;
    getPageInfo(id: number): Promise<PageInfoResult>;
    /**
     * Returns page id if it exists or underfined otherwise
     * @param path path to page or path to directory where page is located
     * @param pagename page iname or empty string if full path to page is provided
     */
    isPageExists(path: string, pagename: string): Promise<number | undefined>;
    /**
     * Creates new page
     * @param page page to create
     */
    createPage(page: AbstractPage): Promise<CreatePageResult>;
    /**
     * Update existing page by replacing it's content
     * @param id page id
     * @param newContent new content to replace
     */
    updatePageContent(id: number, newContent: string): Promise<UpdatePageResult>;
}
