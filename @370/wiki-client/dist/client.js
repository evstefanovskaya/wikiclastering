"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WikiClient = void 0;
const graphql_request_1 = require("graphql-request");
/**
 * Used to interact with Wiki.js GraphQL API
 */
class WikiClient {
    constructor(url, token) {
        this.client = new graphql_request_1.GraphQLClient(url, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
    }
    // Queries
    /**
     * List users by filter
     * @param filter
     * @param orderBy
     */
    listUsers(filter = '', orderBy = '') {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.client.request(graphql_request_1.gql `
            query {
                users {
                    list(
                        filter: "${filter}"
                        orderBy: "${orderBy}"
                    ) {
                        id
                        name
                        email
                        providerKey
                        isSystem
                        isActive
                        createdAt
                        lastLoginAt
                    }
                }
            }`);
        });
    }
    /**
     * Get information about specified user
     * @param id user id
     */
    showUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.client.request(graphql_request_1.gql `
            query {
                users {
                    single(
                        id: ${id}
                    ) {
                        id
                        name
                        email
                        providerKey
                        providerId
                        isSystem
                        isActive
                        isVerified
                        location
                        jobTitle
                        timezone
                        dateFormat
                        appearance
                        createdAt
                        updatedAt
                        lastLoginAt
                        groups {
                            id
                            name
                            isSystem
                            permissions
                            pageRules {
                                id
                                deny
                                match
                                roles
                                path
                                locales
                            }
                            users {
                                id
                                name
                                email
                                providerKey
                                isSystem
                                isActive
                                createdAt
                                lastLoginAt
                            }
                            createdAt
                            updatedAt
                        }
                    }
                }
            }`);
        });
    }
    /**
     * List groups by filter
     * @param filter
     * @param orderBy
     */
    listGroups(filter = '', orderBy = '') {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.client.request(graphql_request_1.gql `
            query {
                groups {
                    list(
                        filter: "${filter}"
                        orderBy: "${orderBy}"
                    ) {
                        id
                        name
                        isSystem
                        userCount
                        createdAt
                        updatedAt
                    }
                }
            }`);
        });
    }
    /**
     * Get information about specified group
     * @param id group id
     */
    showGroup(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.client.request(graphql_request_1.gql `
            query {
                groups {
                    single(
                        id: ${id}
                    ) {
                        id
                        name
                        isSystem
                        permissions
                        pageRules {
                            id
                            deny
                            match
                            roles
                            path
                            locales
                        }
                        users {
                            id
                            name
                            email
                            providerKey
                            isSystem
                            isActive
                            createdAt
                            lastLoginAt
                        }
                        createdAt
                        updatedAt
                    }
                }
            }`);
        });
    }
    getPageInfo(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.client.request(graphql_request_1.gql `
            query {
                pages {
                    single(
                        id: ${id}
                    ) {
                        id
                        path
                        hash
                        title
                        description
                        isPrivate
                        isPublished
                        content
                        editor
                    }
                }
            }
            `);
        });
    }
    /**
     * Returns page id if it exists or underfined otherwise
     * @param path path to page or path to directory where page is located
     * @param pagename page iname or empty string if full path to page is provided
     */
    isPageExists(path, pagename) {
        return __awaiter(this, void 0, void 0, function* () {
            path = path.startsWith('/') ? path.substring(1) : path; // remove leading '/'
            const result = yield this.client.request(graphql_request_1.gql `
            query {
                pages {
                    search(
                        query: "${pagename}"
                        path: "${path}"
                    ) {
                        results {
                            id
                            path
                        }
                    }
                }
            }
            `);
            result.pages.search.results = result.pages.search.results.filter((pg) => {
                return pg.path == path;
            });
            if (result.pages.search.results.length == 0)
                return undefined;
            return parseInt(result.pages.search.results[0].id);
        });
    }
    // Mutations
    /**
     * Creates new page
     * @param page page to create
     */
    createPage(page) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.client.request(graphql_request_1.gql `
            mutation {
                pages {
                    create(
                        content: "${page.content}"
                        description: "${page.description}"
                        editor: "${page.editor}"
                        isPublished: ${page.isPublished}
                        isPrivate: ${page.isPrivate}
                        locale: "${page.locale}"
                        path: "${page.path}"
                        tags: ${page.tags.length ? page.tags : '[]'}
                        title: "${page.title}"
                    ){
                        responseResult {
                            succeeded
                            errorCode
                            slug
                            message
                        }
                    }
                }
            }`);
        });
    }
    /**
     * Update existing page by replacing it's content
     * @param id page id
     * @param newContent new content to replace
     */
    updatePageContent(id, newContent) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.client.request(graphql_request_1.gql `
            mutation {
                pages {
                    update(
                        id: ${id}
                        content: "${newContent}"
                        isPublished: true
                        isPrivate: false
                    ) {
                        responseResult {
                            succeeded
                            errorCode
                            slug
                            message
                        }
                    }
                }
            }
            `);
        });
    }
}
exports.WikiClient = WikiClient;
