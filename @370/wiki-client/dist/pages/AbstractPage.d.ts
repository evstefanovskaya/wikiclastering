export declare type Editor = 'markdown' | 'visual';
export declare type Locale = 'en' | 'ru';
/**
 * Abstract page is a page with arbitary content
 */
export declare class AbstractPage {
    protected _path: string;
    protected _title: string;
    protected _content: string;
    protected _description: string;
    protected _editor: Editor;
    protected _isPublished: boolean;
    protected _isPrivate: boolean;
    protected _locale: Locale;
    protected _tags: Array<string>;
    /**
     * Creates new page with specified parameters
     * @param name page name which will be shown in path
     * @param prefix a part of path before page name
     * @param title page title which will be shown in menu and as title itself
     * @param content page content (format depends on selected editor)
     * @param description page description
     * @param editor editor (or format) which will be used to create page, default - markdown
     * @param isPublished is page published, should always be true
     * @param isPrivate is page private, should always be false
     * @param locale locale of page, default - ru
     * @param tags a list of tacgs to be attached to page
     */
    constructor(name: string, prefix: string, title: string, content: string, description?: string, editor?: Editor, isPublished?: boolean, isPrivate?: boolean, locale?: Locale, tags?: Array<string>);
    get content(): string;
    get description(): string;
    get editor(): Editor;
    get isPublished(): boolean;
    get isPrivate(): boolean;
    get locale(): Locale;
    get path(): string;
    get tags(): Array<string>;
    get title(): string;
}
