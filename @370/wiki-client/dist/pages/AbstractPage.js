"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbstractPage = void 0;
/**
 * Abstract page is a page with arbitary content
 */
class AbstractPage {
    /**
     * Creates new page with specified parameters
     * @param name page name which will be shown in path
     * @param prefix a part of path before page name
     * @param title page title which will be shown in menu and as title itself
     * @param content page content (format depends on selected editor)
     * @param description page description
     * @param editor editor (or format) which will be used to create page, default - markdown
     * @param isPublished is page published, should always be true
     * @param isPrivate is page private, should always be false
     * @param locale locale of page, default - ru
     * @param tags a list of tacgs to be attached to page
     */
    constructor(name, prefix, title, content, description = '', editor = 'markdown', isPublished = true, isPrivate = false, locale = 'ru', tags = []) {
        this._path = `${prefix}/${name}`;
        this._title = title.replace(/"/g, '\\"').replace(/\n/g, '').replace(/\r/g, '');
        this._content = content.replace(/\n/g, '\\n').replace(/"/g, '\\"').replace(/\r/g, '');
        this._description = description.replace(/"/g, '\\"').replace(/\n/g, '').replace(/\r/g, '');
        this._editor = editor;
        this._isPublished = isPublished;
        this._isPrivate = isPrivate;
        this._locale = locale;
        this._tags = tags;
    }
    get content() {
        return this._content;
    }
    get description() {
        return this._description;
    }
    get editor() {
        return this._editor;
    }
    get isPublished() {
        return this._isPublished;
    }
    get isPrivate() {
        return this._isPrivate;
    }
    get locale() {
        return this._locale;
    }
    get path() {
        return this._path;
    }
    get tags() {
        return this._tags;
    }
    get title() {
        return this._title;
    }
}
exports.AbstractPage = AbstractPage;
