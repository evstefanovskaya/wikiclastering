import { Editor, Locale, AbstractPage } from './AbstractPage';
export interface TemplateParams {
    [key: string]: string;
}
export declare class TemplateError extends Error {
    constructor(message: string);
}
declare class TemplatePage extends AbstractPage {
    constructor(name: string, prefix: string, title: string, template: string, params: TemplateParams, description?: string, editor?: Editor, isPublished?: boolean, isPrivate?: boolean, locale?: Locale, tags?: Array<string>);
}
/**
 * TemplateFactory is used to create pages using specified content
 */
export declare class TemplatePageFactory<T extends TemplateParams> {
    private _template;
    /**
     * Creates new TemplateFactory
     * @param templatePath absolute path to .mdt file, which contains valid page template
     */
    constructor(templatePath: string);
    /**
     * Creates page with speciied template arguments and other page parameters.
     * For page parameters, see AbstractPage class
     */
    createPage(name: string, prefix: string, title: string, params: T, description?: string, editor?: Editor, isPublished?: boolean, isPrivate?: boolean, locale?: Locale, tags?: Array<string>): TemplatePage;
}
export {};
