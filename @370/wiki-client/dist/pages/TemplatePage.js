"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemplatePageFactory = exports.TemplateError = void 0;
const fs_1 = require("fs");
const AbstractPage_1 = require("./AbstractPage");
const templateRegex = /{{[$A-Za-z_][0-9A-Za-z_$]*}}/g;
class TemplateError extends Error {
    constructor(message) {
        super(message);
        this.name = 'TemplateError';
    }
}
exports.TemplateError = TemplateError;
function createContent(template, params) {
    let content = template;
    Object.keys(params).forEach((key) => {
        content = content.replace(new RegExp(`{{${key}}}`, 'g'), params[key]);
    });
    const match = templateRegex.exec(content);
    if (match) {
        throw new TemplateError(`Missing value for '${match[0].replace(/[{}]/g, '')}'`);
    }
    return content;
}
class TemplatePage extends AbstractPage_1.AbstractPage {
    constructor(name, prefix, title, template, params, description, editor, isPublished, isPrivate, locale, tags) {
        super(name, prefix, title, createContent(template, params), description, editor, isPublished, isPrivate, locale, tags);
    }
}
/**
 * TemplateFactory is used to create pages using specified content
 */
class TemplatePageFactory {
    /**
     * Creates new TemplateFactory
     * @param templatePath absolute path to .mdt file, which contains valid page template
     */
    constructor(templatePath) {
        this._template = fs_1.readFileSync(templatePath, 'utf-8');
    }
    /**
     * Creates page with speciied template arguments and other page parameters.
     * For page parameters, see AbstractPage class
     */
    createPage(name, prefix, title, params, description, editor, isPublished, isPrivate, locale, tags) {
        return new TemplatePage(name, prefix, title, this._template, params, description, editor, isPublished, isPrivate, locale, tags);
    }
}
exports.TemplatePageFactory = TemplatePageFactory;
