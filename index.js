"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var wiki_client_1 = require("./@370/wiki-client");
var script_1 = require("./script");
var endpoints_config_1 = require("./endpoints.config");
var path = 'claster9';
var prefix = 'Project_400';
(function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, page, pId, e_1, e_2, e_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new wiki_client_1.WikiClient(endpoints_config_1["default"].URL, endpoints_config_1["default"].TOKEN);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 12, , 13]);
                return [4 /*yield*/, script_1.loadPage('claster.html')];
            case 2:
                page = (_a.sent());
                if (!page) return [3 /*break*/, 11];
                return [4 /*yield*/, client.isPageExists(prefix + '/' + path, "")];
            case 3:
                pId = (_a.sent());
                if (!pId) return [3 /*break*/, 8];
                console.log("pId=" + pId);
                _a.label = 4;
            case 4:
                _a.trys.push([4, 6, , 7]);
                console.log("Page updating");
                return [4 /*yield*/, client.updatePageContent(pId, page)];
            case 5:
                _a.sent();
                console.log("Page updated");
                return [3 /*break*/, 7];
            case 6:
                e_1 = _a.sent();
                console.log(e_1.message);
                return [2 /*return*/];
            case 7: return [3 /*break*/, 11];
            case 8:
                _a.trys.push([8, 10, , 11]);
                console.log("Page creating");
                return [4 /*yield*/, client.createPage(new wiki_client_1.AbstractPage(path, prefix, 'Результаты кластеризации', page))];
            case 9:
                _a.sent();
                return [3 /*break*/, 11];
            case 10:
                e_2 = _a.sent();
                console.log(e_2);
                return [2 /*return*/];
            case 11: return [3 /*break*/, 13];
            case 12:
                e_3 = _a.sent();
                return [2 /*return*/, e_3];
            case 13: return [2 /*return*/];
        }
    });
}); })()["catch"](function (err) {
    console.log(err);
});
