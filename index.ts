import { AbstractPage, WikiClient } from "./@370/wiki-client";
import { loadPage } from "./script";
import endpoints from "./endpoints.config";

const path = 'claster9';
const prefix = 'Project_400';
(async () => {
    const client = new WikiClient(endpoints.URL, endpoints.TOKEN);
    try {
        let page = (await loadPage('claster.html'));
        if (page) {
            const pId = (await client.isPageExists(prefix+'/'+path, ""));
            if (pId) {
                console.log("pId=" + pId);
                try {
                    console.log("Page updating");
                    await client.updatePageContent(pId, page);
                    console.log("Page updated");
                } catch (e) {
                    console.log(e.message);
                    return;
                }
            } else {
                try {
                    console.log("Page creating");
                    await client.createPage(new AbstractPage(
                        path,
                        prefix,
                        'Результаты кластеризации',
                        page
                    ));
                } catch (e) {
                    console.log(e);
                    return;
                }
            }
        }
    } catch (e) {
        return e;
    }
})().catch((err) => {
  console.log(err);
});
