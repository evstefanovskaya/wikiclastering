"use strict";
exports.__esModule = true;
exports.loadPage = void 0;
var fs = require("fs");
exports.loadPage = function (name) {
    return fs.readFileSync(name).toString();
};
