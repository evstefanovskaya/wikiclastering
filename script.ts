import fs = require("fs");

export const loadPage = (name: string) : string => {
  return fs.readFileSync(name).toString();
 };
