export interface IUser {
  username: string;
  begin:string;
  end: string;
  duration: string;
  room: string;
  date: string;
}

export interface userData {
  name: string;
  sessions: IUser[];
}